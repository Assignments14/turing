<?php

const BR = '</br>';

class TreeNode
{
    public $data;
    public $left;
    public $right;

    public function __construct($data)
    {
        $this->data = $data; 
    }
}

function deserialize(array $queue)
{
    $nodes = array_map(function($val) {
        if (is_null($val)) {
            return null;
        }
        return new TreeNode(intval($val));
    }, $queue);

    $kids = array_reverse($nodes);
    $root = array_pop($kids);

    foreach($nodes as $node) {
        if ($node) {
            $node->left = array_pop($kids);
            $node->right = array_pop($kids);
        }
    }

    return $root;
}

function build_queue($raw) {
    $queue = [];
    $l = sizeof($raw);

    for ($i = 0; $i < $l; $i++) {
        array_push($queue, $raw[$i] == 'null' ? null : intval($raw[$i]));
    }

    return $queue;
}

class Solution 
{
    public function sum_of_left_leaves(TreeNode $root = null)
    {
        $sum = 0;

        while ($root) {
            if ($root->left) {
                $sum += $root->left->data;
            }
            $root = $root->right;
        }

        return $sum;
    }
}


$input = explode(',', '1,2,3,4,5,6');
$queue = build_queue($input);
$tree = deserialize($queue);

$sol = new Solution();
$output = $sol->sum_of_left_leaves($tree);

echo $output, PHP_EOL;

// class Solution {
//     public function calcPoints($ops)
//     {
//         if (!is_array($ops) || empty($ops)) {
//             return 0;
//         }

//         $result = [];
//         $top_index = -1;

//         foreach ($ops as $op) {

//             switch ($op) {
//                 case is_numeric($op):
//                     $result[] = (int) $op;
//                     $top_index++;
//                     break;
//                 case 'C':
//                     array_pop($result);
//                     $top_index--;
//                     break;
//                 case 'D':
//                     $result[] = $result[$top_index] * 2;
//                     $top_index++;
//                     break;
//                 case '+':
//                     $result[] = $result[$top_index] + $result[$top_index-1];
//                     $top_index++;
//                     break;
//             }
//         }

//         return array_sum($result);
//     }

//     public function isValid($s): bool
//     {
//         $stack = [];

//         $openers = ['[', '(', '{'];
//         $closers = [']', ')', '}'];    

//         $all = array_combine($closers, $openers);

//         $brackets = str_split($s);

//         foreach ($brackets as $bracket) {
//             if (in_array($bracket, $openers)) {
//                 $stack[] = $bracket;
//                 continue;
//             }
//             if (in_array($bracket, $closers) && end($stack) === $all[$bracket]) {
//                 array_pop($stack);
//             }
//         }

//         return empty($stack);
//     }
// }

// $ops = explode(' ', '5 2 C D +');

// $solution = new Solution();
// $output = $solution->calcPoints($ops);

// echo $output, PHP_EOL;

// $output = $solution->isValid('[](){)');

// echo $output ? 'valid' : 'not valid';
